import 'package:dapp/models/collection.dart';
import 'package:dapp/utils/routes.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:web3dart/web3dart.dart';
import 'package:http/http.dart';
import 'package:syncfusion_flutter_sliders/sliders.dart';
import 'dart:convert';
import 'dart:html';
import 'dart:typed_data';
import 'package:web3dart/browser.dart';
import 'package:path/path.dart' show join, dirname;
import 'package:web_socket_channel/io.dart';
import 'package:web3dart/contracts/erc20.dart';
import 'package:http/http.dart' as http;

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String contractAddress = '0x2642b24e7bfc9358bf3844b5b1baf208edd8cfcb';
  late EthPrivateKey credential;
  var credentialMetamasks;
  late Web3Client ethClient;
  late Client httpClient;
  final myAddress = '0x2841A517f81a36bD3C346EB1D33BCd173355c43c';
  int myAmount = 0;
  var myData = 0.0;
  var newAddress = '0x2841A517f81a36bD3C346EB1D33BCd173355c43c';
  var privateKey = '0x2642b24e7bfc9358bf3844b5b1baf208edd8cfcb';
  final rpcUrlInfura =
      'https://ropsten.infura.io/v3/46d601f5d20f4301b12e20956b4ea44d';

  final senderAddress = '0x750A7bB3dBFA2e3eb50Cce2EfBd98d74A9f0AcF4';

  var _etherType;
  double _value = 0.0;

  @override
  void initState() {
    super.initState();
    httpClient = Client();
    // ethClient = Web3Client(
    //     'https://ropsten.infura.io/v3/46d601f5d20f4301b12e20956b4ea44d',
    //     httpClient);
    // getWalletBalance();
  }

  Future<void> connectWallet() async {
    var eth = window.ethereum;
    if (eth != null) {
      var credentialMetamask = await eth.requestAccount();
      setState(() {
        credentialMetamasks = credentialMetamask;
        newAddress = credentialMetamask.address.toString();
        ethClient = Web3Client.custom(eth.asRpcService());
        getWalletBalance();
      });
    }
  }

  Future<void> changeWallet() async {
    var eth = window.ethereum;
    if (eth!.isConnected()) {
      eth.stream('disconnect').cast();
    }
  }

  Future<void> getWalletBalance() async {
    EthereumAddress address = EthereumAddress.fromHex(newAddress);
    EtherAmount balance = await ethClient.getBalance(address);
    double balanceUnit = balance.getValueInUnit(EtherUnit.ether);
    print(balanceUnit);
    setState(() {
      myData = balanceUnit;
    });
  }

  void sendTr() async {
    final client = Web3Client(rpcUrlInfura, Client());

    var test = await client.sendTransaction(
      credentialMetamasks,
      Transaction(
        to: EthereumAddress.fromHex(senderAddress),
        gasPrice: EtherAmount.fromUnitAndValue(_etherType, myAmount + 1),
        maxGas: 100000,
        value: EtherAmount.fromUnitAndValue(_etherType, myAmount),
        maxFeePerGas: EtherAmount.fromUnitAndValue(EtherUnit.wei, myAmount + 1),
        maxPriorityFeePerGas:
            EtherAmount.fromUnitAndValue(_etherType, myAmount + 1),
      ),
      chainId: 3,
    );
    print(test);
    await client.dispose();
  }

  void sendErc() async {
    final client = Web3Client(rpcUrlInfura, Client());

    final shibaInu = Erc20(
      address: EthereumAddress.fromHex(senderAddress),
      client: client,
      chainId: 3,
    );
    final myAddressL = EthereumAddress.fromHex(newAddress);
    final senderAddressL = EthereumAddress.fromHex(senderAddress);
    //  var status = await shibaInu.transferFrom(myAddressL, senderAddressL,BigInt.one, credentials: credentialMetamasks);
    var status = await shibaInu.transfer(senderAddressL, BigInt.from(myAmount),
        credentials: credentialMetamasks);
    print(status);
  }

  void sendContract() async {
    final client = Web3Client(rpcUrlInfura, Client());
    EthereumAddress address = EthereumAddress.fromHex(newAddress);
    final contract =
        DeployedContract(ContractAbi.fromJson(newAddress, 'MetaCoin'), address);

    final transferEvent = contract.event('Transfer');
    final balanceFunction = contract.function('getBalance');
    final sendFunction = contract.function('sendCoin');

    final subscription = client
        .events(FilterOptions.events(contract: contract, event: transferEvent))
        .take(1)
        .listen((result) {
      final decoded = transferEvent.decodeResults(
          result.topics!.toList(), result.data.toString());

      final from = decoded[0] as EthereumAddress;
      final to = decoded[1] as EthereumAddress;
      final value = decoded[2] as BigInt;

      print('$from sent $value MetaCoins to $to');
    });

    final balance = await client.call(
        contract: contract, function: balanceFunction, params: [newAddress]);
    print('We have ${balance.first} MetaCoins');

    await client.sendTransaction(
      credentialMetamasks,
      Transaction.callContract(
        contract: contract,
        function: sendFunction,
        parameters: [senderAddress, balance.first],
      ),
    );

    await subscription.asFuture();
    await subscription.cancel();

    await client.dispose();
  }

  void alertDialog(String title, String content) {
    AlertDialog alertDialog = AlertDialog(
      title: Text(title.toString()),
      content: Text(content.toString()),
      actions: [],
    );
    showDialog(
      context: context,
      builder: (context) {
        return alertDialog;
      },
    );
  }

  // Future<DeployedContract> loadContract() async {
  //   String abi = await rootBundle.loadString("abi.json");
  //   String contractAddress = "0x2642b24e7bfc9358bf3844b5b1baf208edd8cfcb";
  //   final contract = DeployedContract(ContractAbi.fromJson(abi, "hello"),
  //       EthereumAddress.fromHex(contractAddress));
  //   return contract;
  // }

  // Future<List<dynamic>> query(String functionName, List<dynamic> args) async {
  //   final contract = await loadContract();
  //   final ethFunction = contract.function(functionName);
  //   final result = await ethClient.call(
  //       contract: contract, function: ethFunction, params: args);
  //   return result;
  // }

  // Future<String> depositWallet() async {
  //   credential = EthPrivateKey.fromHex(
  //       "8496ec0ce7ef5a530ce5e845f25d798038f399eacc0821d60822e629b7783cbe");
  //   var bigAmount = BigInt.from(myAmount);
  //   var response = await ethClient.sendTransaction(
  //     credential,
  //     Transaction(
  //       from: EthereumAddress.fromHex(myAddress),
  //       to: EthereumAddress.fromHex(senderAddress),
  //       gasPrice: EtherAmount.inWei(BigInt.one),
  //       maxGas: 100000,
  //       value: EtherAmount.fromUnitAndValue(EtherUnit.ether, 1),
  //     ),
  //     chainId: 3,
  //   );
  //   print(bigAmount);
  //   print(response);
  //   return response;
  // }

  // Future<String> depositCoin() async {
  //   var bigAmount = BigInt.from(myAmount);
  //   var response = await submit("deposit", [bigAmount]);
  //   return response;
  // }

  // Future<String> withdrawCoin() async {
  //   var bigAmount = BigInt.from(myAmount);
  //   var response = await submit("withdraw", [bigAmount]);
  //   print(bigAmount);
  //   return response;
  // }

  // Future<String> submit(String functionName, List<dynamic> args) async {
  //   credential = EthPrivateKey.fromHex(
  //       "8496ec0ce7ef5a530ce5e845f25d798038f399eacc0821d60822e629b7783cbe");

  //   DeployedContract contract = await loadContract();
  //   final ethFunction = contract.function(functionName);
  //   final result = await ethClient.sendTransaction(
  //       credential,
  //       Transaction.callContract(
  //           contract: contract,
  //           function: ethFunction,
  //           parameters: args,
  //           maxGas: 1000000),
  //       chainId: 3);
  //   print(result);
  //   return result;
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.only(top: 50),
              margin: EdgeInsets.all(10),
              child: Text(
                '${myData} \ETH',
                style: TextStyle(fontSize: 20),
              ),
            ),
            InkWell(
              child: Container(
                margin: EdgeInsets.only(top: 50),
                height: 40,
                width: 200,
                color: Colors.greenAccent,
                child: Center(
                  child: Text(
                    'REFRESH',
                    style: TextStyle(fontSize: 18, color: Colors.white),
                  ),
                ),
              ),
              onTap: () {
                // getBalance(myAddress);
                getWalletBalance();
              },
            ),
            SfSlider(
              min: 0.0,
              max: 10.0,
              interval: 1,
              showTicks: true,
              showLabels: true,
              enableTooltip: true,
              minorTicksPerInterval: 1,
              value: _value,
              onChanged: (value) {
                setState(
                  () {
                    _value = value;
                    myAmount = value.round();
                  },
                );
              },
            ),
            ListTile(
              title: Text('Ether'),
              leading: Radio(
                value: EtherUnit.ether,
                groupValue: _etherType,
                onChanged: (value) {
                  setState(() {
                    _etherType = value;
                  });
                },
              ),
            ),
            ListTile(
              title: Text('Finney'),
              leading: Radio(
                value: EtherUnit.finney,
                groupValue: _etherType,
                onChanged: (value) {
                  setState(() {
                    _etherType = value;
                  });
                },
              ),
            ),
            ListTile(
              title: Text('Gwei'),
              leading: Radio(
                value: EtherUnit.gwei,
                groupValue: _etherType,
                onChanged: (value) {
                  setState(() {
                    _etherType = value;
                  });
                },
              ),
            ),
            ListTile(
              title: Text('Szabo'),
              leading: Radio(
                value: EtherUnit.szabo,
                groupValue: _etherType,
                onChanged: (value) {
                  setState(() {
                    _etherType = value;
                  });
                },
              ),
            ),
            ListTile(
              title: Text('Wei'),
              leading: Radio(
                value: EtherUnit.wei,
                groupValue: _etherType,
                onChanged: (value) {
                  setState(() {
                    _etherType = value;
                  });
                },
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                InkWell(
                  child: Container(
                    margin: EdgeInsets.only(top: 50),
                    height: 40,
                    width: 200,
                    color: Colors.blueAccent,
                    child: Center(
                        child: Text(
                      "TRANSFER",
                      style: TextStyle(fontSize: 18, color: Colors.white),
                    )),
                  ),
                  onTap: () {
                    // sendErc();
                    sendTr();
                  },
                ),
                InkWell(
                  child: Container(
                    margin: EdgeInsets.only(top: 50),
                    height: 40,
                    width: 200,
                    color: Colors.blueAccent,
                    child: Center(
                        child: Text(
                      "WITHDRAW",
                      style: TextStyle(fontSize: 18, color: Colors.white),
                    )),
                  ),
                  onTap: () {},
                ),
                InkWell(
                  child: Container(
                    margin: EdgeInsets.only(top: 50),
                    height: 40,
                    width: 200,
                    color: Colors.blueAccent,
                    child: Center(
                        child: Text(
                      "CONNECT",
                      style: TextStyle(fontSize: 18, color: Colors.white),
                    )),
                  ),
                  onTap: () {
                    connectWallet();
                  },
                ),
              ],
            ),
            InkWell(
              child: Container(
                margin: EdgeInsets.only(top: 50),
                height: 40,
                width: 200,
                color: Colors.redAccent,
                child: Center(
                    child: Text(
                  "LOGOUT",
                  style: TextStyle(fontSize: 18, color: Colors.white),
                )),
              ),
              onTap: () {
                changeWallet();
              },
            ),
            InkWell(
              child: Container(
                margin: EdgeInsets.only(top: 50),
                height: 40,
                width: 200,
                color: Colors.redAccent,
                child: Center(
                    child: Text(
                  "API TEST",
                  style: TextStyle(fontSize: 18, color: Colors.white),
                )),
              ),
              onTap: () {
                Navigator.pushNamed(context, MyRoutes.homeRoute);
              },
            ),
          ],
        ),
      ),
    );
  }
}
