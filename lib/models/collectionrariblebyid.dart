// To parse this JSON data, do
//
//     final collectionRaribleById = collectionRaribleByIdFromJson(jsonString);

import 'dart:convert';

CollectionRaribleById collectionRaribleByIdFromJson(String str) =>
    CollectionRaribleById.fromJson(json.decode(str));

String collectionRaribleByIdToJson(CollectionRaribleById data) =>
    json.encode(data.toJson());

class CollectionRaribleById {
  CollectionRaribleById({
    this.id,
    this.blockchain,
    this.type,
    this.name,
    this.symbol,
    this.owner,
    this.features,
    this.minters,
    this.meta,
    this.originOrders,
  });

  String? id;
  String? blockchain;
  String? type;
  String? name;
  String? symbol;
  String? owner;
  List<String>? features;
  List<dynamic>? minters;
  Meta? meta;
  List<dynamic>? originOrders;

  factory CollectionRaribleById.fromJson(Map<String, dynamic> json) =>
      CollectionRaribleById(
        id: json["id"] == null ? null : json["id"],
        blockchain: json["blockchain"] == null ? null : json["blockchain"],
        type: json["type"] == null ? null : json["type"],
        name: json["name"] == null ? null : json["name"],
        symbol: json["symbol"] == null ? null : json["symbol"],
        owner: json["owner"] == null ? null : json["owner"],
        features: json["features"] == null
            ? null
            : List<String>.from(json["features"].map((x) => x)),
        minters: json["minters"] == null
            ? null
            : List<dynamic>.from(json["minters"].map((x) => x)),
        meta: json["meta"] == null ? null : Meta.fromJson(json["meta"]),
        originOrders: json["originOrders"] == null
            ? null
            : List<dynamic>.from(json["originOrders"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "blockchain": blockchain == null ? null : blockchain,
        "type": type == null ? null : type,
        "name": name == null ? null : name,
        "symbol": symbol == null ? null : symbol,
        "owner": owner == null ? null : owner,
        "features": features == null
            ? null
            : List<dynamic>.from(features!.map((x) => x)),
        "minters":
            minters == null ? null : List<dynamic>.from(minters!.map((x) => x)),
        "meta": meta == null ? null : meta!.toJson(),
        "originOrders": originOrders == null
            ? null
            : List<dynamic>.from(originOrders!.map((x) => x)),
      };
}

class Meta {
  Meta({
    this.name,
    this.description,
    this.tags,
    this.genres,
    this.externalUri,
    this.content,
    this.externalLink,
    this.sellerFeeBasisPoints,
  });

  String? name;
  String? description;
  List<dynamic>? tags;
  List<dynamic>? genres;
  String? externalUri;
  List<Content>? content;
  String? externalLink;
  int? sellerFeeBasisPoints;

  factory Meta.fromJson(Map<String, dynamic> json) => Meta(
        name: json["name"] == null ? null : json["name"],
        description: json["description"] == null ? null : json["description"],
        tags: json["tags"] == null
            ? null
            : List<dynamic>.from(json["tags"].map((x) => x)),
        genres: json["genres"] == null
            ? null
            : List<dynamic>.from(json["genres"].map((x) => x)),
        externalUri: json["externalUri"] == null ? null : json["externalUri"],
        content: json["content"] == null
            ? null
            : List<Content>.from(
                json["content"].map((x) => Content.fromJson(x))),
        externalLink:
            json["externalLink"] == null ? null : json["externalLink"],
        sellerFeeBasisPoints: json["sellerFeeBasisPoints"] == null
            ? null
            : json["sellerFeeBasisPoints"],
      );

  Map<String, dynamic> toJson() => {
        "name": name == null ? null : name,
        "description": description == null ? null : description,
        "tags": tags == null ? null : List<dynamic>.from(tags!.map((x) => x)),
        "genres":
            genres == null ? null : List<dynamic>.from(genres!.map((x) => x)),
        "externalUri": externalUri == null ? null : externalUri,
        "content": content == null
            ? null
            : List<dynamic>.from(content!.map((x) => x.toJson())),
        "externalLink": externalLink == null ? null : externalLink,
        "sellerFeeBasisPoints":
            sellerFeeBasisPoints == null ? null : sellerFeeBasisPoints,
      };
}

class Content {
  Content({
    this.type,
    this.url,
    this.representation,
  });

  String? type;
  String? url;
  String? representation;

  factory Content.fromJson(Map<String, dynamic> json) => Content(
        type: json["@type"] == null ? null : json["@type"],
        url: json["url"] == null ? null : json["url"],
        representation:
            json["representation"] == null ? null : json["representation"],
      );

  Map<String, dynamic> toJson() => {
        "@type": type == null ? null : type,
        "url": url == null ? null : url,
        "representation": representation == null ? null : representation,
      };
}
