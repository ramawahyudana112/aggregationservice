// To parse this JSON data, do
//
//     final collection = collectionFromJson(jsonString);

import 'dart:convert';

Collection collectionFromJson(String str) => Collection.fromJson(json.decode(str));

String collectionToJson(Collection data) => json.encode(data.toJson());

class Collection {
    Collection({
        this.collections,
    });

    List<CollectionElement>? collections;

    factory Collection.fromJson(Map<String, dynamic> json) => Collection(
        collections: json["collections"] == null ? null : List<CollectionElement>.from(json["collections"].map((x) => CollectionElement.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "collections": collections == null ? null : List<dynamic>.from(collections!.map((x) => x.toJson())),
    };
}

class CollectionElement {
    CollectionElement({
        this.primaryAssetContracts,
        this.traits,
        // this.stats,
        this.bannerImageUrl,
        this.chatUrl,
        this.createdDate,
        this.defaultToFiat,
        this.description,
        this.devBuyerFeeBasisPoints,
        this.devSellerFeeBasisPoints,
        this.discordUrl,
        this.displayData,
        this.externalUrl,
        this.featured,
        this.featuredImageUrl,
        this.hidden,
        this.safelistRequestStatus,
        this.imageUrl,
        this.isSubjectToWhitelist,
        this.largeImageUrl,
        this.mediumUsername,
        this.name,
        this.onlyProxiedTransfers,
        this.openseaBuyerFeeBasisPoints,
        this.openseaSellerFeeBasisPoints,
        this.payoutAddress,
        this.requireEmail,
        this.shortDescription,
        this.slug,
        this.telegramUrl,
        this.twitterUsername,
        this.instagramUsername,
        this.wikiUrl,
        this.isNsfw,
    });

    List<dynamic>? primaryAssetContracts;
    Traits? traits;
    // Map<String, int>? stats;
    dynamic bannerImageUrl;
    dynamic chatUrl;
    DateTime? createdDate;
    bool? defaultToFiat;
    String? description;
    String? devBuyerFeeBasisPoints;
    String? devSellerFeeBasisPoints;
    dynamic discordUrl;
    DisplayData? displayData;
    dynamic externalUrl;
    bool? featured;
    dynamic? featuredImageUrl;
    bool? hidden;
    SafelistRequestStatus? safelistRequestStatus;
    String? imageUrl;
    bool? isSubjectToWhitelist;
    dynamic largeImageUrl;
    dynamic mediumUsername;
    String? name;
    bool? onlyProxiedTransfers;
    String? openseaBuyerFeeBasisPoints;
    String? openseaSellerFeeBasisPoints;
    dynamic payoutAddress;
    bool? requireEmail;
    dynamic? shortDescription;
    String? slug;
    dynamic telegramUrl;
    dynamic twitterUsername;
    dynamic instagramUsername;
    dynamic wikiUrl;
    bool? isNsfw;

    factory CollectionElement.fromJson(Map<String, dynamic> json) => CollectionElement(
        primaryAssetContracts: json["primary_asset_contracts"] == null ? null : List<dynamic>.from(json["primary_asset_contracts"].map((x) => x)),
        traits: json["traits"] == null ? null : Traits.fromJson(json["traits"]),
        // stats: json["stats"] == null ? null : Map.from(json["stats"]).map((k, v) => MapEntry<String, int>(k, v)),
        bannerImageUrl: json["banner_image_url"],
        chatUrl: json["chat_url"],
        createdDate: json["created_date"] == null ? null : DateTime.parse(json["created_date"]),
        defaultToFiat: json["default_to_fiat"] == null ? null : json["default_to_fiat"],
        description: json["description"] == null ? null : json["description"],
        devBuyerFeeBasisPoints: json["dev_buyer_fee_basis_points"] == null ? null : json["dev_buyer_fee_basis_points"],
        devSellerFeeBasisPoints: json["dev_seller_fee_basis_points"] == null ? null : json["dev_seller_fee_basis_points"],
        discordUrl: json["discord_url"],
        displayData: json["display_data"] == null ? null : DisplayData.fromJson(json["display_data"]),
        externalUrl: json["external_url"],
        featured: json["featured"] == null ? null : json["featured"],
        featuredImageUrl: json["featured_image_url"],
        hidden: json["hidden"] == null ? null : json["hidden"],
        safelistRequestStatus: json["safelist_request_status"] == null ? null : safelistRequestStatusValues.map![json["safelist_request_status"]],
        imageUrl: json["image_url"] == null ? null : json["image_url"],
        isSubjectToWhitelist: json["is_subject_to_whitelist"] == null ? null : json["is_subject_to_whitelist"],
        largeImageUrl: json["large_image_url"],
        mediumUsername: json["medium_username"],
        name: json["name"] == null ? null : json["name"],
        onlyProxiedTransfers: json["only_proxied_transfers"] == null ? null : json["only_proxied_transfers"],
        openseaBuyerFeeBasisPoints: json["opensea_buyer_fee_basis_points"] == null ? null : json["opensea_buyer_fee_basis_points"],
        openseaSellerFeeBasisPoints: json["opensea_seller_fee_basis_points"] == null ? null : json["opensea_seller_fee_basis_points"],
        payoutAddress: json["payout_address"],
        requireEmail: json["require_email"] == null ? null : json["require_email"],
        shortDescription: json["short_description"],
        slug: json["slug"] == null ? null : json["slug"],
        telegramUrl: json["telegram_url"],
        twitterUsername: json["twitter_username"],
        instagramUsername: json["instagram_username"],
        wikiUrl: json["wiki_url"],
        isNsfw: json["is_nsfw"] == null ? null : json["is_nsfw"],
    );

    Map<String, dynamic> toJson() => {
        "primary_asset_contracts": primaryAssetContracts == null ? null : List<dynamic>.from(primaryAssetContracts!.map((x) => x)),
        "traits": traits == null ? null : traits!.toJson(),
        // "stats": stats == null ? null : Map.from(stats!).map((k, v) => MapEntry<String, dynamic>(k, v)),
        "banner_image_url": bannerImageUrl,
        "chat_url": chatUrl,
        "created_date": createdDate == null ? null : createdDate!.toIso8601String(),
        "default_to_fiat": defaultToFiat == null ? null : defaultToFiat,
        "description": description == null ? null : description,
        "dev_buyer_fee_basis_points": devBuyerFeeBasisPoints == null ? null : devBuyerFeeBasisPoints,
        "dev_seller_fee_basis_points": devSellerFeeBasisPoints == null ? null : devSellerFeeBasisPoints,
        "discord_url": discordUrl,
        "display_data": displayData == null ? null : displayData!.toJson(),
        "external_url": externalUrl,
        "featured": featured == null ? null : featured,
        "featured_image_url": featuredImageUrl,
        "hidden": hidden == null ? null : hidden,
        "safelist_request_status": safelistRequestStatus == null ? null : safelistRequestStatusValues.reverse[safelistRequestStatus],
        "image_url": imageUrl == null ? null : imageUrl,
        "is_subject_to_whitelist": isSubjectToWhitelist == null ? null : isSubjectToWhitelist,
        "large_image_url": largeImageUrl,
        "medium_username": mediumUsername,
        "name": name == null ? null : name,
        "only_proxied_transfers": onlyProxiedTransfers == null ? null : onlyProxiedTransfers,
        "opensea_buyer_fee_basis_points": openseaBuyerFeeBasisPoints == null ? null : openseaBuyerFeeBasisPoints,
        "opensea_seller_fee_basis_points": openseaSellerFeeBasisPoints == null ? null : openseaSellerFeeBasisPoints,
        "payout_address": payoutAddress,
        "require_email": requireEmail == null ? null : requireEmail,
        "short_description": shortDescription,
        "slug": slug == null ? null : slug,
        "telegram_url": telegramUrl,
        "twitter_username": twitterUsername,
        "instagram_username": instagramUsername,
        "wiki_url": wikiUrl,
        "is_nsfw": isNsfw == null ? null : isNsfw,
    };
}

class DisplayData {
    DisplayData({
        this.cardDisplayStyle,
        this.images,
    });

    CardDisplayStyle? cardDisplayStyle;
    List<dynamic>? images;

    factory DisplayData.fromJson(Map<String, dynamic> json) => DisplayData(
        cardDisplayStyle: json["card_display_style"] == null ? null : cardDisplayStyleValues.map![json["card_display_style"]],
        images: json["images"] == null ? null : List<dynamic>.from(json["images"].map((x) => x)),
    );

    Map<String, dynamic> toJson() => {
        "card_display_style": cardDisplayStyle == null ? null : cardDisplayStyleValues.reverse[cardDisplayStyle],
        "images": images == null ? null : List<dynamic>.from(images!.map((x) => x)),
    };
}

enum CardDisplayStyle { CONTAIN }

final cardDisplayStyleValues = EnumValues({
    "contain": CardDisplayStyle.CONTAIN
});

enum SafelistRequestStatus { NOT_REQUESTED }

final safelistRequestStatusValues = EnumValues({
    "not_requested": SafelistRequestStatus.NOT_REQUESTED
});

class Traits {
    Traits();

    factory Traits.fromJson(Map<String, dynamic> json) => Traits(
    );

    Map<String, dynamic> toJson() => {
    };
}

class EnumValues<T> {
    Map<String, T>? map;
    Map<T, String>? reverseMap;

    EnumValues(this.map);

    Map<T, String> get reverse {
        if (reverseMap == null) {
            reverseMap = map!.map((k, v) => new MapEntry(v, k));
        }
        return reverseMap!;
    }
}
