// To parse this JSON data, do
//
//     final rarible = raribleFromJson(jsonString);

import 'dart:convert';

Rarible raribleFromJson(String str) => Rarible.fromJson(json.decode(str));

String raribleToJson(Rarible data) => json.encode(data.toJson());

class Rarible {
    Rarible({
        this.total,
        this.continuation,
        this.collections,
    });

    int? total;
    String? continuation;
    List<CollectionRarible>? collections;

    factory Rarible.fromJson(Map<String, dynamic> json) => Rarible(
        total: json["total"] == null ? null : json["total"],
        continuation: json["continuation"] == null ? null : json["continuation"],
        collections: json["collections"] == null ? null : List<CollectionRarible>.from(json["collections"].map((x) => CollectionRarible.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "total": total == null ? null : total,
        "continuation": continuation == null ? null : continuation,
        "collections": collections == null ? null : List<dynamic>.from(collections!.map((x) => x.toJson())),
    };
}

class CollectionRarible {
    CollectionRarible({
        this.id,
        this.blockchain,
        this.type,
        this.name,
        this.symbol,
        this.owner,
        this.features,
        this.minters,
        this.meta,
        this.originOrders,
        this.bestBidOrder,
    });

    String? id;
    Blockchain? blockchain;
    TypeEnum? type;
    String? name;
    String? symbol;
    String? owner;
    List<Feature>? features;
    List<String>? minters;
    Meta? meta;
    List<dynamic>? originOrders;
    BestBidOrder? bestBidOrder;

    factory CollectionRarible.fromJson(Map<String, dynamic> json) => CollectionRarible(
        id: json["id"] == null ? null : json["id"],
        blockchain: json["blockchain"] == null ? null : blockchainValues.map![json["blockchain"]],
        type: json["type"] == null ? null : typeEnumValues.map![json["type"]],
        name: json["name"] == null ? null : json["name"],
        symbol: json["symbol"] == null ? null : json["symbol"],
        owner: json["owner"] == null ? null : json["owner"],
        features: json["features"] == null ? null : List<Feature>.from(json["features"].map((x) => featureValues.map![x])),
        minters: json["minters"] == null ? null : List<String>.from(json["minters"].map((x) => x)),
        meta: json["meta"] == null ? null : Meta.fromJson(json["meta"]),
        originOrders: json["originOrders"] == null ? null : List<dynamic>.from(json["originOrders"].map((x) => x)),
        bestBidOrder: json["bestBidOrder"] == null ? null : BestBidOrder.fromJson(json["bestBidOrder"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "blockchain": blockchain == null ? null : blockchainValues.reverse[blockchain],
        "type": type == null ? null : typeEnumValues.reverse[type],
        "name": name == null ? null : name,
        "symbol": symbol == null ? null : symbol,
        "owner": owner == null ? null : owner,
        "features": features == null ? null : List<dynamic>.from(features!.map((x) => featureValues.reverse[x])),
        "minters": minters == null ? null : List<dynamic>.from(minters!.map((x) => x)),
        "meta": meta == null ? null : meta!.toJson(),
        "originOrders": originOrders == null ? null : List<dynamic>.from(originOrders!.map((x) => x)),
        "bestBidOrder": bestBidOrder == null ? null : bestBidOrder!.toJson(),
    };
}

class BestBidOrder {
    BestBidOrder({
        this.id,
        this.fill,
        this.platform,
        this.status,
        this.makeStock,
        this.cancelled,
        this.createdAt,
        this.lastUpdatedAt,
        this.dbUpdatedAt,
        this.takePrice,
        this.takePriceUsd,
        this.maker,
        this.make,
        this.take,
        this.salt,
        this.signature,
        this.pending,
        this.data,
        this.endedAt,
    });

    String? id;
    String? fill;
    String? platform;
    String? status;
    String? makeStock;
    bool? cancelled;
    DateTime? createdAt;
    DateTime? lastUpdatedAt;
    DateTime? dbUpdatedAt;
    String? takePrice;
    String? takePriceUsd;
    String? maker;
    Ake? make;
    Ake? take;
    String? salt;
    String? signature;
    List<dynamic>? pending;
    Data? data;
    DateTime? endedAt;

    factory BestBidOrder.fromJson(Map<String, dynamic> json) => BestBidOrder(
        id: json["id"] == null ? null : json["id"],
        fill: json["fill"] == null ? null : json["fill"],
        platform: json["platform"] == null ? null : json["platform"],
        status: json["status"] == null ? null : json["status"],
        makeStock: json["makeStock"] == null ? null : json["makeStock"],
        cancelled: json["cancelled"] == null ? null : json["cancelled"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        lastUpdatedAt: json["lastUpdatedAt"] == null ? null : DateTime.parse(json["lastUpdatedAt"]),
        dbUpdatedAt: json["dbUpdatedAt"] == null ? null : DateTime.parse(json["dbUpdatedAt"]),
        takePrice: json["takePrice"] == null ? null : json["takePrice"],
        takePriceUsd: json["takePriceUsd"] == null ? null : json["takePriceUsd"],
        maker: json["maker"] == null ? null : json["maker"],
        make: json["make"] == null ? null : Ake.fromJson(json["make"]),
        take: json["take"] == null ? null : Ake.fromJson(json["take"]),
        salt: json["salt"] == null ? null : json["salt"],
        signature: json["signature"] == null ? null : json["signature"],
        pending: json["pending"] == null ? null : List<dynamic>.from(json["pending"].map((x) => x)),
        data: json["data"] == null ? null : Data.fromJson(json["data"]),
        endedAt: json["endedAt"] == null ? null : DateTime.parse(json["endedAt"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "fill": fill == null ? null : fill,
        "platform": platform == null ? null : platform,
        "status": status == null ? null : status,
        "makeStock": makeStock == null ? null : makeStock,
        "cancelled": cancelled == null ? null : cancelled,
        "createdAt": createdAt == null ? null : createdAt!.toIso8601String(),
        "lastUpdatedAt": lastUpdatedAt == null ? null : lastUpdatedAt!.toIso8601String(),
        "dbUpdatedAt": dbUpdatedAt == null ? null : dbUpdatedAt!.toIso8601String(),
        "takePrice": takePrice == null ? null : takePrice,
        "takePriceUsd": takePriceUsd == null ? null : takePriceUsd,
        "maker": maker == null ? null : maker,
        "make": make == null ? null : make!.toJson(),
        "take": take == null ? null : take!.toJson(),
        "salt": salt == null ? null : salt,
        "signature": signature == null ? null : signature,
        "pending": pending == null ? null : List<dynamic>.from(pending!.map((x) => x)),
        "data": data == null ? null : data!.toJson(),
        "endedAt": endedAt == null ? null : endedAt!.toIso8601String(),
    };
}

class Data {
    Data({
        this.type,
        this.payouts,
        this.originFees,
    });

    String? type;
    List<dynamic>? payouts;
    List<OriginFee>? originFees;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        type: json["@type"] == null ? null : json["@type"],
        payouts: json["payouts"] == null ? null : List<dynamic>.from(json["payouts"].map((x) => x)),
        originFees: json["originFees"] == null ? null : List<OriginFee>.from(json["originFees"].map((x) => OriginFee.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "@type": type == null ? null : type,
        "payouts": payouts == null ? null : List<dynamic>.from(payouts!.map((x) => x)),
        "originFees": originFees == null ? null : List<dynamic>.from(originFees!.map((x) => x.toJson())),
    };
}

class OriginFee {
    OriginFee({
        this.account,
        this.value,
    });

    String? account;
    int? value;

    factory OriginFee.fromJson(Map<String, dynamic> json) => OriginFee(
        account: json["account"] == null ? null : json["account"],
        value: json["value"] == null ? null : json["value"],
    );

    Map<String, dynamic> toJson() => {
        "account": account == null ? null : account,
        "value": value == null ? null : value,
    };
}

class Ake {
    Ake({
        this.type,
        this.value,
    });

    TypeClass? type;
    String? value;

    factory Ake.fromJson(Map<String, dynamic> json) => Ake(
        type: json["type"] == null ? null : TypeClass.fromJson(json["type"]),
        value: json["value"] == null ? null : json["value"],
    );

    Map<String, dynamic> toJson() => {
        "type": type == null ? null : type!.toJson(),
        "value": value == null ? null : value,
    };
}

class TypeClass {
    TypeClass({
        this.type,
        this.contract,
    });

    String? type;
    String? contract;

    factory TypeClass.fromJson(Map<String, dynamic> json) => TypeClass(
        type: json["@type"] == null ? null : json["@type"],
        contract: json["contract"] == null ? null : json["contract"],
    );

    Map<String, dynamic> toJson() => {
        "@type": type == null ? null : type,
        "contract": contract == null ? null : contract,
    };
}

enum Blockchain { POLYGON, ETHEREUM, SOLANA, TEZOS }

final blockchainValues = EnumValues({
    "ETHEREUM": Blockchain.ETHEREUM,
    "POLYGON": Blockchain.POLYGON,
    "SOLANA": Blockchain.SOLANA,
    "TEZOS": Blockchain.TEZOS
});

enum Feature { APPROVE_FOR_ALL, SET_URI_PREFIX, BURN, MINT_WITH_ADDRESS, SECONDARY_SALE_FEES, MINT_AND_TRANSFER }

final featureValues = EnumValues({
    "APPROVE_FOR_ALL": Feature.APPROVE_FOR_ALL,
    "BURN": Feature.BURN,
    "MINT_AND_TRANSFER": Feature.MINT_AND_TRANSFER,
    "MINT_WITH_ADDRESS": Feature.MINT_WITH_ADDRESS,
    "SECONDARY_SALE_FEES": Feature.SECONDARY_SALE_FEES,
    "SET_URI_PREFIX": Feature.SET_URI_PREFIX
});

class Meta {
    Meta({
        this.name,
        this.tags,
        this.genres,
        this.content,
        this.description,
        this.externalUri,
        this.externalLink,
        this.sellerFeeBasisPoints,
        this.feeRecipient,
    });

    Name? name;
    List<dynamic>? tags;
    List<dynamic>? genres;
    List<Content>? content;
    String? description;
    String? externalUri;
    String? externalLink;
    int? sellerFeeBasisPoints;
    String? feeRecipient;

    factory Meta.fromJson(Map<String, dynamic> json) => Meta(
        name: json["name"] == null ? null : nameValues.map![json["name"]],
        tags: json["tags"] == null ? null : List<dynamic>.from(json["tags"].map((x) => x)),
        genres: json["genres"] == null ? null : List<dynamic>.from(json["genres"].map((x) => x)),
        content: json["content"] == null ? null : List<Content>.from(json["content"].map((x) => Content.fromJson(x))),
        description: json["description"] == null ? null : json["description"],
        externalUri: json["externalUri"] == null ? null : json["externalUri"],
        externalLink: json["externalLink"] == null ? null : json["externalLink"],
        sellerFeeBasisPoints: json["sellerFeeBasisPoints"] == null ? null : json["sellerFeeBasisPoints"],
        feeRecipient: json["feeRecipient"] == null ? null : json["feeRecipient"],
    );

    Map<String, dynamic> toJson() => {
        "name": name == null ? null : nameValues.reverse[name],
        "tags": tags == null ? null : List<dynamic>.from(tags!.map((x) => x)),
        "genres": genres == null ? null : List<dynamic>.from(genres!.map((x) => x)),
        "content": content == null ? null : List<dynamic>.from(content!.map((x) => x.toJson())),
        "description": description == null ? null : description,
        "externalUri": externalUri == null ? null : externalUri,
        "externalLink": externalLink == null ? null : externalLink,
        "sellerFeeBasisPoints": sellerFeeBasisPoints == null ? null : sellerFeeBasisPoints,
        "feeRecipient": feeRecipient == null ? null : feeRecipient,
    };
}

class Content {
    Content({
        this.type,
        this.url,
        this.representation,
    });

    String? type;
    String? url;
    String? representation;

    factory Content.fromJson(Map<String, dynamic> json) => Content(
        type: json["@type"] == null ? null : json["@type"],
        url: json["url"] == null ? null : json["url"],
        representation: json["representation"] == null ? null : json["representation"],
    );

    Map<String, dynamic> toJson() => {
        "@type": type == null ? null : type,
        "url": url == null ? null : url,
        "representation": representation == null ? null : representation,
    };
}

enum Name { UNTITLED, DIGI_DAIGAKU_GENESIS_LANDS, COLLECTION_NFT, VIGNETTES, PROOF_OF_STAKE_BOOKS }

final nameValues = EnumValues({
    "Collection NFT": Name.COLLECTION_NFT,
    "DigiDaigaku Genesis LANDS": Name.DIGI_DAIGAKU_GENESIS_LANDS,
    "Proof of Stake: Books": Name.PROOF_OF_STAKE_BOOKS,
    "Untitled": Name.UNTITLED,
    "vignettes.": Name.VIGNETTES
});

enum TypeEnum { ERC721, ERC1155, SOLANA, TEZOS_MT }

final typeEnumValues = EnumValues({
    "ERC1155": TypeEnum.ERC1155,
    "ERC721": TypeEnum.ERC721,
    "SOLANA": TypeEnum.SOLANA,
    "TEZOS_MT": TypeEnum.TEZOS_MT
});

class EnumValues<T> {
    Map<String, T>? map;
    Map<T, String>? reverseMap;

    EnumValues(this.map);

    Map<T, String> get reverse {
        if (reverseMap == null) {
            reverseMap = map!.map((k, v) => new MapEntry(v, k));
        }
        return reverseMap!;
    }
}
