import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;

import 'models/collection.dart';
import 'models/rarible.dart';
import 'models/collectionrariblebyid.dart';
import 'package:cached_network_image/cached_network_image.dart';

class openSeaContainer extends StatefulWidget {
  openSeaContainer({Key? key}) : super(key: key);

  @override
  State<openSeaContainer> createState() => _openSeaContainerState();
}

class _openSeaContainerState extends State<openSeaContainer> {
  CollectionRarible? col;
  Collection? collect;
  CollectionRaribleById? collectionById;
  List? collectionByIdList = [];
  bool isLoading = true;
  bool isReadyToGet = false;
  Rarible? rarible;
  List? topCollectionId = [];
  List collectionList = [];

  @override
  void initState() {
    super.initState();
    // fetchDataOpenSea();
    // fetchDataRarible();
    fetchDataTopRarible();
  }

  Future<List<dynamic>> getData() async {
    http.Response response = await http.get(
        Uri.parse(
            'https://api.opensea.io/api/v1/collections?offset=0&limit=10'),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        });
    var store = json.decode(response.body);
    // print(store['collections']);
    return store['collections'];
  }

  // fetchDataOpenSea() async {
  //   try {
  //     http.Response response = await http.get(
  //         Uri.parse(
  //             'https://api.opensea.io/api/v1/collections?offset=0&limit=100'),
  //         headers: {
  //           'Content-Type': 'application/json',
  //           'Accept': 'application/json',
  //         });
  //     if (response.statusCode == 200) {
  //       var result = jsonDecode(response.body);
  //       print(result);
  //       if (response.body != null) {
  //         setState(() {
  //           collect = collectionFromJson(response.body);
  //           print(collect);
  //           isLoading = false;
  //         });
  //       }
  //       print('ttt');
  //     }
  //   } catch (e) {
  //     print('ERROR : $e');
  //   }
  // }

  fetchDataTopRarible() async {
    try {
      http.Response response = await http.get(
          Uri.parse('https://api.rarible.org/v0.1/data/trending/collections'),
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
          });
      if (response.statusCode == 200) {
        setState(() {
          var decode = jsonDecode(response.body);
          topCollectionId!.addAll(decode);
        });
        fetchDataRaribleById();
      }
    } catch (e) {
      print(e.toString());
    }
  }

  fetchDataRaribleById() async {
    try {
      var i = 0;
      List collectionItem = [];
      if (topCollectionId!.isNotEmpty == true && topCollectionId!.length >= 0) {
        topCollectionId!.forEach(
          (element) async {
            http.Response response = await http.get(
                Uri.parse(
                    'https://api.rarible.org/v0.1/collections/${element["id"]}'),
                headers: {
                  'Content-Type': 'application/json',
                  'Accept': 'application/json',
                });

            if (response.statusCode == 200) {
              collectionItem.add(collectionRaribleByIdFromJson(response.body));
              setState(() {
                collectionList = collectionItem;
              });
            }
          },
        );
        isLoading = false;
      } else {
        print('not ready');
        isLoading = false;
      }
    } catch (e) {
      print('ERROR : $e');
      isLoading = false;
    }
  }

  testIsiData() {
    print(collectionList);
  }

  // fetchDataRarible() async {
  //   try {
  //     http.Response response = await http.get(
  //         Uri.parse('https://api.rarible.org/v0.1/collections/all'),
  //         headers: {
  //           'Content-Type': 'application/json',
  //           'Accept': 'application/json',
  //         });
  //     if (response.statusCode == 200) {
  //         setState(() {
  //           rarible = raribleFromJson(response.body);
  //           isLoading = false;
  //         });
  //     }
  //   } catch (e) {
  //     print('ERROR : $e');
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var height = size.height;
    var width = size.width;
    return Scaffold(
      appBar: AppBar(title: Text('OPENSEA API')),
      // body: Center(
      //   child: Column(
      //     children: [
      //       // NavigationBar(),
      //       // isLoading == false
      //       // ? Flexible(
      //       //   child: ListView.builder(
      //       //       itemCount: collect!.collections!.length,
      //       //       itemBuilder: (context, index) {
      //       //         if (collect!.collections![index] != null) {
      //       //           var item = collect!.collections![index];
      //       //           if (item.imageUrl == null) {
      //       //             return Container();
      //       //           }
      //       //           return ListTile(
      //       //             leading: CachedNetworkImage(imageUrl: item.imageUrl.toString(),fit: BoxFit.fitWidth, width: 100),
      //       //             title: Text(item.name.toString()),
      //       //             subtitle: Text(
      //       //                 'Floor Price: ${item.createdDate}'),
      //       //             trailing:
      //       //                 Text(item.openseaSellerFeeBasisPoints.toString()),
      //       //           );
      //       //         } else {
      //       //           return CircularProgressIndicator();
      //       //         }
      //       //       }),
      //       // )
      //       // ? Flexible(
      //       //     child: ListView.builder(
      //       //         itemCount: rarible!.collections!.length,
      //       //         itemBuilder: (context, index) {
      //       //           if (rarible!.collections![index] != null) {
      //       //             var item = rarible!.collections![index];
      //       //             if (item.meta == null) {
      //       //               return Container();
      //       //             }
      //       //             if (item.meta!.content == null ||
      //       //                 item.meta!.content!.length == 0) {
      //       //               return Container();
      //       //             }
      //       //             return ListTile(
      //       //               leading: CachedNetworkImage(
      //       //                   imageUrl:
      //       //                       item.meta!.content!.first.url.toString(),
      //       //                   fit: BoxFit.fitWidth,
      //       //                   width: 100),
      //       //               title: Text(item.name.toString()),
      //       //               subtitle: Text('Floor Price: ${item.id}'),
      //       //               trailing: Text(item.blockchain.toString()),
      //       //             );
      //       //           } else {
      //       //             return CircularProgressIndicator();
      //       //           }
      //       //         }),
      //       //   )
      //       // : CircularProgressIndicator(),
      //       ElevatedButton(onPressed: () => testIsiData(), child: Text('test'))
      //     ],
      //   ),
      // ),
      backgroundColor: Colors.white,
      body: isLoading != true
          ? ListView.builder(
              itemCount: collectionList.length,
              itemBuilder: (BuildContext context, int index) {
                return Card(
                  child: Row(children: [
                    Text('${collectionList[index].meta.name}'),
                  ]),
                );
              },
            )
          : Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Center(child: CircularProgressIndicator()),
              ],
            ),
    );
  }
}

//Navigation Bar

class NavigationBar extends StatelessWidget {
  const NavigationBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var height = size.height;
    var width = size.width;
    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(width: 1.0, color: Colors.black)),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(
            width: width / 4,
          ),
          SizedBox(
            width: width / 4,
          ),
          NavigationItem(
            title: "Explore",
          ),
          NavigationItem(
            title: "Stats",
          ),
          NavigationItem(
            title: "Resources",
          ),
          NavigationItem(
            title: "Create",
          ),
          Icon(Icons.supervised_user_circle_rounded, color: Colors.grey),
          Icon(Icons.wallet, color: Colors.grey[350]),
        ],
      ),
    );
  }
}

class NavigationItem extends StatelessWidget {
  NavigationItem({Key? key, this.title}) : super(key: key);

  String? title;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(10.0),
      child: GestureDetector(
        onTap: () {},
        child: Text(
          'haha',
          style: GoogleFonts.notoSans(fontSize: 16),
        ),
      ),
    );
  }
}
