class MyRoutes{
  static String mobileRoute = '/mobile';
  static String homeRoute = '/home';
  static String webRoute = '/web';
  static String initRoute = '/main';
}