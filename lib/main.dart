import 'package:dapp/pages/login_page.dart';
import 'package:dapp/utils/routes.dart';
import 'package:flutter/material.dart';
import 'opensea.dart';
// import 'webtest.dart';
import 'mobile.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Testing',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: MyRoutes.initRoute,
      routes: {
        MyRoutes.mobileRoute: (context) => LoginPage(),
        // MyRoutes.webRoute: (context) => MyHomePage(title: "Web Test"),
        MyRoutes.initRoute: (context) => BottomNavBar(),
        MyRoutes.homeRoute: (context) => openSeaContainer(),
      },
    );
  }
}
